<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>MPI | Accueil</title>
	<link rel="stylesheet" type="text/css" href="CSS/index.css">
</head>
<body>

<nav class="bgc1 clearfix">
    <h1 class="mhmm mvm">
		<span>
            <a style="color:black;" href="index.php">MPI</a> > Contact
        </span>
			<span>
				<a style="float: right; margin-right: 0.5em;" href="cours/linux.php">Linux</a>
			</span>
			<span>	
				<a style="float: right; margin-right: 1em;" href="cours/network.php">Réseau</a>
			</span>
			<span>	
				<a style="float: right; margin-right: 1em;" href="cours/dev.php">Développement</a>
			</span>
	</h1>
</nav>


<h2 style="text-align: center;">Comment me contacter ?</h2>
    <p style="text-align: center;">Pour me contacter vous pouvez m'envoyer un message sur mon compte twitter, <a href="https://twitter.com/zamKdev" target="_blank">en cliquant ici</a></p>
    <p style="text-align: center;">Ou alors m'envoyer un <a href="mailto:matheo.paw@gmail.com">Mail</a> !</p>
    





</body>
</html>