<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>MPI | Linux</title>
	<link rel="stylesheet" type="text/css" href="../CSS/summary.css">
	<link rel="stylesheet" href="../fonts/icomoon/style.css">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,700,900"> 

</head>
<body>
	<nav class="bgc1 clearfix">
        <h1 class="mhmm mvm">
            <span>
                <a style="color:black;" href="../index.php">MPI</a> > Linux
			</span>
			<span>
				<a style="float: right; margin-right: 0.5em;" href="linux.php">Linux</a>
			</span>
			<span>	
				<a style="float: right; margin-right: 1em;" href="network.php">Réseau</a>
			</span>
			<span>	
				<a style="float: right; margin-right: 1em;" href="dev.php">Développement</a>
			</span>      
        </h1>
	</nav>


	<p class="TitleUPCenter"><span class="icon-angle-right"></span> Arch Linux</p>
		<ul class="List">
			<li><a href="linux/Arch/Install.php">Comment installer Arch Linux</a></li>
			<li><a href="linux/Arch/desktopm.php">Les environnements de bureau</a>	</li>
		</ul>

	<p class="TitleUPCenter"><span class="icon-angle-right"></span> Serveur</p>
		<ul class="List">
			<li><a href="linux/server/apache.php">Créer un serveur Web en utilisant Ubuntu Serveur 16.04</a></li>
		</ul>
	
</body>
</html>