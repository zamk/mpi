<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <title>MPI | Linux</title>
    <link rel="stylesheet" type="text/css" href="../../../CSS/cours.css">
    <link rel="stylesheet" type="text/css" href="../../../fonts/icomoon/style.css">

</head>
<body>
    <nav class="bgc1 clearfix">
        <h1 class="mhmm mvm">
            <span>
                <a style="color:black;" href="../../../index.php">MPI</a> > <a style="color:black;" href="../../linux.php">Linux</a> > Arch 
            </span>
            <span>
				<a style="float: right; margin-right: 0.5em;" href="../../linux.php">Linux</a>
			</span>
			<span>	
				<a style="float: right; margin-right: 1em;" href="../../network.php">Réseau</a>
			</span>
			<span>	
				<a style="float: right; margin-right: 1em;" href="../../dev.php">Développement</a>
            </span>        
        </h1>
    </nav>

<h1 style="text-align: center;">Les environnements de bureau</h1>
    <p>Je vais couper ce chapitre en plusieurs étape, tout d’abord je vais vous expliquer ce qu’est un environnement de bureau, puis je vais procéder en vous présentant différents environnements et pour finir je vais vous montrer comment en installer un pour Arch.</p>

<h1>Un environnement de bureau, c'est quoi?</h1>
    <p>L'environnement de bureau est une suite de programmes combinés pour donner une interface cohérente a l'ordinateur. Elle contient en général un programme affichant une barre, une zone de notification, des icônes servant de raccourci, un bureau.</p>
    <p>De nombreux systèmes d'exploitation ont un environnement de bureau incorporé. À l'inverse, avec le système de fenêtrage X des systèmes d'exploitation Unix, plusieurs environnements de bureau sont disponibles.</p>
    <p style="text-align: center;">Voice quelque exemple d'environnement de bureau</p>
    <img class="archIMG" src="../../../image/environnement de bureau/gnome.png">
    <p style="text-align: center;">Environnement de bureau GNOME, un des plus connu et utilisé.</p>
    <img class="archIMG" src="../../../image/environnement de bureau/Unity.png">
    <p style="text-align: center;">Unity, il a été conçu pour un usage aisé sur les écrans tactiles.</p>
    <img class="archIMG" src="../../../image/environnement de bureau/Mate.png">
    <p style="text-align: center;">MATE, fork de GNOME 2, rendu populaire par Linux Mint et sa similitude avec Windows10.</p>
    <hr>
        <h1>Comment installer GNOME sur Arch Linux</h1>
            <p>Dans cette partie du chapitre je vais vous montrer comment installer GNOME sur votre machine Arch Linux.</p>
                <p>Avant de commencer, cette partit du chapitre est réservé à ceux qui ont suivi l'installation d'Arch Linux, si vous n'avez pas suivi le chapitre précédent, vous ne pourrez pas réaliser l'installation de GNOME.</p>
                <hr>
                <p>Tout d'abord, assurez-vous que vous avez mis à jour votre système Arch Linux.</p>
                    <code>sudo pacman -Syu</code>
                <p>Après la mise à jour, redémarrez Arch Linux pour appliquer les dernières mises à jour.</p>
                    <code>sudo reboot</code>
                <p>Ensuite, installez X (xorg) en utilisant la commande :</p>
                    <code>sudo pacman -S xorg xorg-server</code>
                <p>Enfin, installez l'environnement de bureau GNOME en utilisant la commande :</p>
                    <code>sudo pacman -S gnome</code>
                <p>Cette commande installera toutes les applications nécessaires, y compris le gestionnaire d'affichage gnome pour l'environnement de bureau GNOME.</p><br>
                <p>Démarrer et activer le service gdm :</p>
                    <code>sudo systemctl start gdm.service</code>
                    <code>sudo systemctl enable gdm.service</code>
                <p>Enfin, redémarrez votre système Arch pour vous connecter à l'environnement de bureau GNOME.</p>
                    <code>sudo reboot</code>
                <p>Et voila, vous avez installer GNOME sur votre machine Arch Linux</p> <br>
                <p>Si vous avez des problèmes de son, installez le paquet PulseAudio :</p>
                    <code>sudo pacman -S pulseaudio pulseaudio-alsa</code> <br>
                    <hr>






</body>
</html>