<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <title>MPI | Linux</title>
    <link rel="stylesheet" type="text/css" href="../../../CSS/cours.css">
    <link rel="stylesheet" type="text/css" href="../../../fonts/icomoon/style.css">

</head>
<body>
    <nav class="bgc1 clearfix">
        <h1 class="mhmm mvm">
            <span>
                <a style="color:black;" href="../../../index.php">MPI</a> > <a style="color:black;" href="../../linux.php">Linux</a> > Arch 
            </span>
            <span>
				<a style="float: right; margin-right: 0.5em;" href="../../linux.php">Linux</a>
			</span>
			<span>	
				<a style="float: right; margin-right: 1em;" href="../../network.php">Réseau</a>
			</span>
			<span>	
				<a style="float: right; margin-right: 1em;" href="../../dev.php">Développement</a>
            </span>        
        </h1>
    </nav>

	<h1 style="text-align: center;">Comment installer Arch Linux</h1>
        <h3>Étape 1 : Télécharger l'ISO Arch Linux</h3>
    <p class="right">Vous pouvez télécharger l'ISO sur le site officiel.</p>
    <a href="https://www.archlinux.org/download/" target="_blank" style="text-decoration: black;"><button class="telecharger">Télécharger l'ISO ici!</button></a>
        <h3>Étape 2 : Créer une clé USB bootable d'Arch Linux</h3>
            <p class="right">Pour créer une clé bootable Arch Linux il vous faudras utilisé le logiciel rufus</p>
            <a href="https://github.com/pbatard/rufus/releases/download/v3.10/rufus-3.10.exe" target="_blank" style="text-decoration: black;"><button class="telecharger">Télécharger Rufus ici</button></a>
            <p class="right">Avant tout, il vous faudra une clé USB de 4 gb minimums. Attention, lors de la création de la clée bootable toutes les donnés seront supprimés.</p>
                <hr>
            <p>Pour commencer sélectionnez votre clée USB sur rufus.</p>
            <img class="rufusimg" src="../../../image/rufus/selectUSB.png">
            <p>Ensuite, sélectionnez votre fichier ISO puis cliquer sur démarrer.</p>
            <img class="rufusimg" src="../../../image/rufus/selectISO.png">
            <p>A la fin de la barre de chargement, vous pouvez retirez la clée USB.</p>
            <hr>
        <h3>Étape 3 : Installation de Arch Linux</h3>
            <p class="right">Commençons donc par l’installation de base de notre système Arch linux.
                Je vous laisse donc boot dessus, et nous commencerons ce tutoriel lorsque vous serez arrivés à l’écran suivant :</p>
                <img class="archIMG" src="../../../image/arch/1.png">
                    <p>On Boot donc Arch Linux (x86_64). A savoir que nous serons automatiquement connectés en root le temps de la configuration de base du système.
                        Attention, le clavier de base est en qwerty, pas très pratique en effet. Nous allons donc le passer en azerty avec la commande suivante :</p>
                       <code>loadkeys fr</code>
                    <p>Avant d’aller plus loin, je vous invite à tester votre connexion internet afin d’éviter de vous retrouver bloqués au milieu de ce tutoriel :
                    </p>
                        <code>ping google.com</code>
        <h4>Étape 3.1 : Partitionnement du disque</h4>
                    <p>La première chose à faire est de partitionner notre disque dur, c’est-à-dire de le diviser en plusieurs régions (nos partitions donc ), afin d’organiser de manière convenable les données . Nous devons commencer par récupérer les informations de notre disque dur .Pour ceci nous allons checker la table de partition :</p>
                        <code>fdisk -l</code>
                            <img src="../../../image/arch/fdisk/2.png" class="fdisk">
                <p>Nous voyons ici que ma machine virtuelle dispose d’un disque dur, nommé sda, d’une taille de 8 GiB . Voici comment je vais partitionner mon disque, à adapter bien évidemment</p>
                    <p class="right"><B>► Une partition de root de 5GiB en sda1 </B> qui sera bootable, c’est-à-dire qu’il contiendra le système d’exploitation entre autres chargé au démarrage</p>    
                    <p class="right"><B>► Une partition swap de 1GiB (taille de la RAM) en sda2.</B> La partition swap est très importante: en effet si votre RAM venait à saturer, cette partition viendrait alors la décharger, empêchant ainsi votre système de planter</p>
                    <p class="right"><B>► Une partition logique qui sera le home en sda3 d’environ 4GiB</B></p>
                        <p>Pour réaliser ceci ,on utilise :</p>
                        <code>cfdisk</code>
                <p>Nous voyons que nous avons le choix entre plusieurs labels qui représentent chacun un format de partition. 
                            Nous allons nous intéresser aux plus courant, les formats <B>DOS</B> et <B>GPT</B>.</p>
                    <p class="right">► <B>DOS</B> est utilisé pour formater les disques durs de taille raisonnable puisque la limite d’une partition s’élève à 2,2 To 
                                . De plus le nombre de partitions maximum est de 4 seulement.</p>
                    <p class="right">► <B>GPT</B> lui est beaucoup plus adapté pour le stockage massif de données puisque la limite par partition s’élève à 256 To et 
                                le nombre de partitions maximum peut aller jusqu’à 128.</p>
                <p>Pour ma part, je vais continuer le tutoriel avec le format <B>DOS</B>, dont les performances suffiront pour la plupart de vos besoins.</p>
                    <p class="right">► <B>Création de sda1 :</B> nous sélectionnons new , nous précisons la taille, dans mon cas, 5G , le type de partition : primary , puis nous sélectionnons l’option bootable pour bien indiquer que cette partition contiendra le système d’exploitation à charger au démarrage.</p>
                    <p class="right">► <B>Création de sda2 :</B> De même nous sélectionnons new , nous précisons sa taille ,dans mon cas toujours, 1G et enfin le type de partition : primary </p>
                    <p class="right">► <B>Création de sda3 :</B> De même nous sélectionnons new , nous précisons sa taille ,dans mon cas toujours, 4G et enfin le type de partition : primary </p>
                <p>Nous n’oublions pas de sélectionner l’option <B>write</B> pour appliquer les modifications au disque puis nous quittons avec l’option quit.</p>
                            <img src="../../../image/arch/fdisk/3.png" class="fdisk">
                <p>Ensuite, il nous faut encore formater et préparer les partitions : Pour sda1 et sda3 , on choisira le système de fichier le plus récent , <B>ext4</B> :</p>
                        <code>mkfs.ext4   /dev/sda1 <br>
                            mkfs.ext4   /dev/sda3</code>
                            <img src="../../../image/arch/fdisk/4.png" class="fdisk">
                <p>Ensuite nous nous occupons de sda2, qui est notre <B>partition de swap</B> rappelons-le . Nous préparons la partition avec :</p>
                        <code>mkswap /dev/sda2</code>
                <p>Puis nous activons l’espace de swap avec :</p>
                        <code>swapon  /dev/sda2</code>
                <p>Nous allons monter les partitions dans le dossier mnt , qui est le point de montage par defaut :</p>
                <p>Nous montons d’abord sda1 dans <B>/mnt :</B></p>
                        <code>mount /dev/sda1  /mnt</code>
                <p>Nous créons le dossier enfant de mnt , <B>home</B> pour en faire le point de montage de sda3: </p>
                        <code>mkdir /mnt/home</code>
                <p>Nous montons sda3 :</p>
                        <code>mount /dev/sda3  /mnt/home</code>
                <p>Nous en avons maintenant fini avec la preparation de notre disque dur, 
                    nous allons donc pouvoir passer à l’installation de base du système .</p>
            <h4>Étape 3.2 : Installation du système</h4>
                <p>Nous commençons par nous procurer les <B>paquets de base</B> qui nous permettrons d’installer le système :</p>
                        <code>pacstrap /mnt base base-devel linux linux-firmware</code>
                <p>Ensuite, nous créons <B>le fichier fstab</B> qui contient la table qui permet au système, lorsqu’il démarre, 
                    de savoir comment utiliser chacune des partitions afin de les intégrer à l’arborescence du système de fichier global :</p>
                        <code>genfstab  /mnt >>  /mnt/etc/fstab</code>
                <p>Nous passons dans la nouvellement créée base du système avec :</p>
                        <code>arch-chroot  /mnt  /bin/bash</code>
                        <img src="../../../image/arch/system/5.png" class="fdisk">
                <p>Nous configurons ensuite le langage du système :</p>
                <p>Nous éditons le fichier de configuration <b>local.gen</b> :</p>
                        <code>nano  /etc/locale.gen</code>
                <p>Nous décommentons la ligne <b>fr_FR.UTF-8 UTF-8</b> .</p>
                <p>Nous générons le langage système :</p>
                        <code>locale-gen</code>
                <p>Nous créons le fichier de configuration <b>/etc/locale.conf</b> et nous y plaçons la ligne suivante :</p>
                        <code>LANG=fr_FR.UTF-8</code>
                <p>Nous voulons par défaut utiliser <b> un clavier français</b>, nous éditons donc <b>/etc/vconsole.conf</b> et nous y mettons :</p>
                        <code>KEYMAP=fr-latin <br>
                            FONT=lat9w-16</code>
                <p>Ceci nous évitera de repasser en qwerty à chaque redémarrage du système.</p>
                <p>Nous assignons au système le fuseau horaire duquel nous dépendons. Pour le trouver :</p>
                        <code>ls /usr/share/zoneinfo/</code>
                <p>Dans notre cas, nous dépendons du fuseau horaire <b>Europe/Paris</b>.</p>
                <p>Nous supprimons le fichier <b>localtime</b> déjà existant :</p>
                        <code>rm /etc/localtime</code>
                <p>Nous créons donc un lien symbolique entre le fichier contenant les informations de notre fuseau horaire vers le dossier de configuration etc :</p>
                        <code>ln -s /usr/share/zoneinfo/Europe/Paris  /etc/localtime</code>
                <p>Nous passons l’horloge du système à la norme UTC :</p>
                        <code>hwclock --systohc --utc</code>
                <p>Nous sécurisons le mode super utilisateur avec un mot de passe :</p>
                        <code>passwd</code>
                <p>Nous éditons le fichier <b>/etc/hostname</b> et nous y renseignons le nom de la machine linux désiré.</p>
            <h4>Étape 3.3 : Configuration du réseau</h4>
                <p>Nous activons le <b>DHCP</b> permettant ainsi l'attribution automatique d'IP :</p>
                    <code>systemctl enable dhcpcd</code>
            <h4>Étape 3.4 : Installation du Grub</h4>
                <p>Nous installons le <b>boot loader</b> qui nous permettra de charger l'OS au démarage de la machine :</p>
                    <code>pacman -S grub os-prober <br> grub-install /dev/sda <br> grub-mkconfig -o /boot/grub/grub.cfg</code>
                <p>Nous quittons le <b>chroot</b> :</p>
                    <code>exit</code>
                <p>Nous démontons sda1 et sda3 en prenant bien soin de démonter l’enfant sda3 avant le parent sda1</p>
                    <code>umount  /mnt/home <br> umount /mnt</code>
              
                <p>Pour finir nous pouvons redémarrer notre système :</p>
                    <code>reboot</code>
                    <img src="../../../image/arch/6.png" class="fdisk">
                <p>Si vous arrivez à cet écran, 
                    félicitation, Arch linux est correctement installé sur votre machine !
                    Voyons encore quelques éléments de configuration de base 
                    qui pourraient s’avérer très utiles. </p>
            <h4>Étape 3.5 : Création d'un utilisateur</h4>
                <p>Pour le moment, seul le compte utilisateur root existe : nous sommes donc toujours en mode super utilisateur ce qui pourrait éventuellement poser des problèmes de sécurité .</p>
                <p>Créons donc un nouvel utilisateur :</p>
                    <code>useradd -g users nomUtilisateur</code>
                <p>Nous ajoutons un mot de passe à ce compte utilisateur :</p>
                    <code>passwd nomUtilisateur</code>
                <p>Nous créons le dossier nomUtilisateur dans home pour cet utilisateur et nous nommons l’utilisateur nomUtilisateur propriétaire de ce fichier :</p>
                    <code>mkdir /home/nomUtilisateur <br>
                    chown nomUtilisateur   /home/nomUtilisateur</code>
                <p>Nous inscrivons l’utilisateur nomUtilisateur dans le fichier de configuration de sudo pour lui donner le droit d’utiliser sudo :</p>
                    <code>visudo ( vérifie la syntaxe du fichier avant de sauvegarder les modifications )</code>
                <p>Nous fermons la session root et nous pouvons dès lors nous connecter avec notre nouvel utilisateur tout fraichement créé !</p>
            <h4>Étape 3.6 : Synchroniser l’horloge système grâce aux serveurs de temps</h4>
                        <p>Il peut arriver que l’horloge de votre système ne soit pas synchroniser avec l’heure actuelle, ce qui peut être l’origine de divers problèmes. La commande suivante permet de vérifier cela :</p>
                           <code>timedatectl</code>
                        <p>Afin de nous assurer d’avoir une horloge système à la bonne heure, nous allons utiliser le protocole NTP (Network Time Protocol ) qui permet de synchroniser notre horloge avec différents serveurs de temps . Installons donc le paquet :</p> 
                            <code>pacman -S ntp</code>
                        <p>Nous cherchons le groupe de serveurs de temps adapté à notre fuseau horaire sur pool.ntp.org puis nous éditons le fichier de configuration de ntp :</p>
                            <code>nano /etc/ntp.conf</code>
                        <p>Nous modifions le groupe de serveur de temps avec ceux trouvés sur le site cité précédemment :</p>
                            <code>serveur 0.fr.pool.ntp.org iburst <br>
                                serveur 1.fr.pool.ntp.org iburst <br>
                                serveur 2.fr.pool.ntp.org iburst <br>
                                serveur 3.fr.pool.ntp.org iburst
                            </code>
                        <p>Note : l’option iburst permet de synchroniser l’horloge avec les serveurs de temps seulement lorsque la connexion au réseau s’est bien effectuée. Nous lançons la synchronisation avec :</p>
                            <code>ntpd -p</code>
                        <p>Il est en revanche plus intéressant d’automatiser le lancement de ntp avec la commande suivante :</p>
                            <code>systemctl enable ntpd.service</code>
                        <p>Voilà pour ce qui est de <b>l’installation basique de Arch linux.</b> Pour ceux désirant aller plus loin, nous allons maintenant voir comment <b><a href="wm.php">installer un environnement de bureau</b></a>
                        </b></p>
</body>
</html>