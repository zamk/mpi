<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <title>MPI | Linux</title>
    <link rel="stylesheet" type="text/css" href="../../../CSS/cours.css">
    <link rel="stylesheet" type="text/css" href="../../../fonts/icomoon/style.css">

</head>
<body>
    <nav class="bgc1 clearfix">
        <h1 class="mhmm mvm">
            <span>
                <a style="color:black;" href="../../../index.php">MPI</a> > <a style="color:black;" href="../../linux.php">Linux</a> > Apache 
            </span>
            <span>
				<a style="float: right; margin-right: 0.5em;" href="../../linux.php">Linux</a>
			</span>
			<span>	
				<a style="float: right; margin-right: 1em;" href="../../network.php">Réseau</a>
			</span>
			<span>	
				<a style="float: right; margin-right: 1em;" href="../../dev.php">Développement</a>
            </span>        
        </h1>
    </nav>


<h1 style="text-align: center;">Créer un serveur web sur Ubuntu Server 16.04</h1>
    <p>Dans ce cour je vais vous montrer comment créer un serveur web avec apache sur Ubuntu Server 16.04</p>
        <h3>Télecharger l'ISO de Ubuntu Server 16.04</h3>
            <p>Pour commencer il vous faudra télécharger l'ISO d’Ubuntu Server depuis le site internet d’Ubuntu</p>
                <a href="https://releases.ubuntu.com/16.04.7/" target="_blank" style="text-decoration: black;"><button class="telecharger">Télécharger l'ISO ici</button></a> <br>
                   <p>Ensuite vous devrez créer une clé bootable, vous pouvez utiliser <a href="https://github.com/pbatard/rufus/releases/download/v3.10/rufus-3.10.exe">rufus</a> pour créer votre clé bootable.</p> 
                   <p>Après avoir créé votre clé bootable, booter sur la clé sur la machine sur laquelle vous voulez faire le server web.</p> <hr>
                   <h3>Comment installer Ubuntu server</h3>
                    <p>Pour installer Ubuntu Server il vous suffira de suivre la video suivante.</p>
                       <iframe style="margin-left: 30em;" width="720" height="480" src="https://www.youtube.com/embed/Rcw-MWh_Dco" frameborder="0" allow="aaccelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            <p style="text-align: center;">Une fois l'installation d'Ubuntu Server terminer, nous passerons à la suite du tutoriel.</p>
<hr>
<h3>Apache et php</h3>
    <p>Après avoir terminé l'installation d'Ubuntu Server nous allons installer Apache et Php.</p>
        <p>Avant de commencer mettez à jour le système, pour cela effectuer la commande suivante</p>
            <code>sudo apt-get update</code>
                <p>suivi de</p>
            <code>sudo apt-get dist-upgrade</code>
<p>Après avoir mis à jour le système nous allons installer Apache et Php</p>
    <p>Pour installer Apache effectuer la commande suivante :</p>
        <code>sudo apt install apache2</code>
            <p>suivi de cette commande pour installer php</p>
                <code>sudo apt install php</code> <br>
<hr> <p>Après avoir tout installé, vérifier que le service Apache est actif. Pour cela effectuer la commande suivante :</p>
    <code>systemctl status apache2</code>
    <p>Après avoir effectué cette commande vous pourrez vérifier si le service est actif, s'il est actif vous verrez comme ci-dessous "active (Running) en vert"</p>
        <img class="archIMG" src="../../../image/serverApache/ApacheRunning.png">
    <p>si ce n'est pas le cas, il vous suffira de start le service manuellement.</p>
        <code>sudo systemctl start apache2</code>
<hr>
    <p>Après avoir installer Apache et php verifier que tout fonctionne. Pour cela executer la commande</p> 
        <code>ip a</code>  
            <p>Cette commande va nous permettre de trouver l'adresse locale de la machine et par la suite nous pourrons essayer de nous connecter à cette adresse pour voir si tout fonctionne correctement.</p>
                <img class="archIMG" src="../../../image/serverApache/ip_a.png">
                    <p style="text-align: center">Essayer maintenant de rentrer cette adresse IP dans un navigateur internet, attention il faut que la machine sur laquelle vous allez effectuer le test soit connecter au même réseau que le serveur.</p>
                        <img class="archIMG" src="../../../image/serverApache/apachepage.png">
                            <p style="text-align: center;">Si cette page s'affiche lorsque vous rentrez votre adresse IP dans votre navigateur internet, alors l'installation a fonctionné</p>
<br><hr>
<h4>Comment changer la page HTML par défaut</h4>
    <p>Pour modifier la page HTML il vous suffira de rentrer dans le dossier ou se trouve les fichiers du site internet. Pour cela rentrer dans le dossier "/var/www/html"</p>
        <code>cd /var/www/html</code>
    <p>Ensuite il ne vous reste plus qu'à vous occuper de votre site comme vous le souhaiter.</p>


        
                        


























</body>
</html>