<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <title>MPI | Développement</title>
    <link rel="stylesheet" type="text/css" href="../CSS/summary.css">
    <link rel="stylesheet" type="text/css" href="../fonts/icomoon/style.css">

</head>
<body>
    <nav class="bgc1 clearfix">
        <h1 class="mhmm mvm">
            <span>
                <a style="color:black;" href="../index.php">MPI</a> > Développement 
            </span>
            <span>
				<a style="float: right; margin-right: 0.5em;" href="linux.php">Linux</a>
			</span>
			<span>	
				<a style="float: right; margin-right: 1em;" href="network.php">Réseau</a>
			</span>
			<span>	
				<a style="float: right; margin-right: 1em;" href="dev.php">Développement</a>
            </span>        
        </h1>
    </nav>

        <p class="TitleUPCenter"><span class="icon-angle-right"></span> HTML</p>
            <ul class="List">
                <li>Chapitre 1</li>
                <li>Chapitre 2</li>
                <li>Chapitre 3</li>
                <li>Chapitre 4</li>
            </ul>
        <p class="TitleUPCenter"><span class="icon-angle-right"></span> CSS</p>
            <ul class="List">
                <li><a href="dev/CSS/Implementing.php">Mettre en place le CSS</a></li>
                <li><a href="dev/CSS/formatagetext.php">Formatage du texte</a></li>
                <li>Chapitre 3</li>
                <li>Chapitre 4</li>
            </ul>
        <p class="TitleUPCenter"><span class="icon-angle-right"></span> PHP</p>
            <ul class="List">
                <li>Chapitre 1</li>
                <li>Chapitre 2</li>
                <li>Chapitre 3</li>
                <li>Chapitre 4</li>
            </ul>
            
</body>
</html>