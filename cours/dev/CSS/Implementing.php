<!DOCTYPE html>
<html>
<head>
	<?php include "include/design.php"; ?>
</head>
<body>
	<?php include "include/headercss.php"; ?>


<h1 style="text-align: center;">Mettre en place le CSS</h1>
<p>CSS (Cascading Style Sheets), c'est cet autre langage qui vient compléter le HTML</p>
<p>Le CSS permet de choisir la couleur de votre texte, de sélectionner la police utilisée sur votre site. Il permet de définir la taille du texte, les bordures, le fond ainsi que de faire la mise en page de votre site</p>
<p>Ce sont les navigateurs web qui font le travail le plus complexe : ils doivent lire le code CSS et comprendre comment afficher la page.</p>
<p>Les navigateurs ne connaissent pas toutes les propriétés CSS qui existent. Plus le navigateur est vieux, moins il connaît de fonctionnalités CSS.</p>
<p>Si le navigateur ne connaît pas une propriété CSS, il l'ignore et ne met pas en forme</p>
<h3>Où écrit-on le CSS ?</h3>
<p> On peut écrire du code en langage CSS à trois endroits différents :</p>
<ul>
	<li style="margin-left: 3em">Dans un fichier .css (méthode la plus recommandée);</li>
	<li style="margin-left: 3em">Dans l'en-tête <b>&lthead&gt</b> du fichier HTML ;</li>
	<li style="margin-left: 3em">Directement dans les balises du fichier HTML via un attribut style (méthode la moins recommandée).</li>
</ul>
<p>On écrit le plus souvent le code CSS dans un fichier spécial ayant l'extension .css
C'est la méthode la plus pratique et la plus souple car cela évite de tout mélanger dans un même fichier.</p>
<h3>Dans un fichier .css  (recommandé)</h3>
	<p>Pour connecter le fichier CSS au fichier HTML il vous faudras utiliser cette balise :</p>
		<code>&ltlink rel="stylesheet" href="style.css"&gt </code>
<h3>Dans l'en-tête &lthead&gt du fichier HTML</h3>
	<p>Pour écrire du CSS directement dans la page HTML voulu, il vous faudra écrire cette balise dans la partie &lthead&gt de la page.</p>
		<code>&ltstyle&gt <br> <br> <b>écriver votre code CSS ici!</b> <br><br> &lt/style&gt</code>
<h3>Directement dans les balises (non recommandé)</h3>
	<p>Dernière méthode, à manipuler avec précaution.
Vous pouvez ajouter un attribut style à n'importe quelle balise.
Vous insérerez votre code CSS directement dans cet attribut :
</p>
<span>
	<p>Exemple :</p> <code>&ltp style="color: blue;"&gt<b>Votre paragraphe</b>&lt/p&gt</code>
</span>
<h3>Quelle méthode choisir ?</h3>
	<p>Il est impératif de prendre l'habitude de travailler avec la première méthode parce que c'est celle utilisée par la majorité des webmasters.</p>
	<p>Car si vous placez le code CSS directement dans le fichier HTML, il faudra copier ce code dans tous les fichiers HTML de votre site !</p>
	<br>
	<p>Si vous désirez faire évoluer votre site et que vous voulez par exemple que vos paragraphes soient écrits en rouge et non en bleu, il faudra modifier chaque fichier HTML un à un, comme le montre la figure suivante.</p>
		<img src="../../../image/css/1_css.jpg" class="archIMG">
			<p style="text-align: center;">Dans ce cas le code CSS est répété dans chaque fichier HTML.</p> <br>
		<p>Si vous travaillez avec un fichier CSS externe, vous n'aurez besoin d'écrire cette instruction qu'une seule fois pour tout votre site, comme le montre la figure suivante.</p>
		<img src="../../../image/css/2_css.jpg" class="archIMG">
			<p style="text-align: center; margin-right: 2.9em;">Le code CSS est donné une fois pour toutes dans un fichier CSS.</p>
		<br><br>
<h3>Appliquer un style à une balise</h3>
	<p>Maintenant que nous savons où placer le code CSS, intéressons-nous de plus près à ce code.</p>
		<code>p  { <br> color: blue; <br> }</code>
	<p>Dans un code CSS comme celui-ci, on trouve trois éléments différents :</p>
		<ul>
			<li><b>Des noms de balises :</b >on écrit les noms des balises dont on veut modifier l'apparence. Par exemple, si je veux modifier l'apparence de tous les paragraphes &ltp&gt, je dois écrire p.</li>
			<li><b>Des propriétés CSS : </b>les « effets de style » de la page sont rangés dans des propriétés. <br>Il y a par exemple la propriété color qui permet d'indiquer la couleur du texte, font-size    qui permet d'indiquer la taille du texte, etc.</li>
			<li><b>Les valeurs :</b> : pour chaque propriété CSS, on doit indiquer une valeur. Par exemple, pour la propriété color, il faut indiquer le nom de la couleur. Pour font-size, il faut indiquer la taille que l’on veut, etc.</li>
		</ul>
			<p>Schématiquement, une feuille de style CSS ressemble donc à cela :</p>

			<code>
				<b>balise1</b> { <br>propriété1: valeur1; <br> propriété2: valeur2; <br>propriété3: valeur3; <br> } <br>
				<b>balise2</b> { <br>propriété1: valeur1; <br> propriété2: valeur2; <br>propriété3: valeur3; <br> propriété4: valeur3; <br> } <br>
				<b>balise3</b> { <br>propriété1: valeur1; <br> } <br>
			</code>
			<p>On écrit le nom de la <b>balise </b>(par exemple h1) et on ouvre des <b>accolades</b> pour, à l'intérieur mettre les <b>propriétés</b> et <b>valeurs</b> que l'on souhaite.</p>
			<p>On peut mettre autant de propriétés que l'on veut à l'intérieur des accolades. <b>Chaque propriété</b> est suivie du symbole <b>"deux-points" (:)</b> puis de la valeur correspondante. Enfin, chaque ligne se termine par un <b>point-virgule (;)</b>.</p>
<h3>Appliquer un style à plusieurs balises.</h3>
	<p>Prenons le code CSS suivant :</p>
		<code>h1 { <br>color: blue; <br>} <br> em { <br>color: blue; <br>}</code>
			<p>Il signifie que nos titres &lth1&gt et nos textes importants &ltem&gt doivent s'afficher en bleu.
Il existe un moyen en CSS d'aller plus vite si les deux balises doivent avoir la même présentation. Il suffit de combiner la déclaration en séparant les noms des balises par une virgule, comme ceci :</p>
	<code>h1, em {<br>color: blue; <br>}</code>
<h3>Commentaires dans du CSS</h3>
	<p>Comme en HTML, il est possible de mettre des commentaires.
Les commentaires ne seront pas affichés, ils servent simplement à indiquer des informations pour vous
Donc, pour faire un commentaire Tapez  /*, suivi de votre commentaire, puis */             pour terminer votre commentaire.</p>
<br>
<p>Vos commentaires peuvent être écrits sur une ou plusieurs lignes. Par exemple :</p>
	<code>p { <br>color: blue; /* Les paragraphes seront en bleu */ <br>}</code>
<h3>Appliquer un style : class et id</h3>
	<p>L’inconvénient d’appliquer un style à une balise c’est que TOUS les paragraphes possèdent la même présentation</p>
		<p>Si on désire que seulement certains paragraphes soient écrits d'une manière différente, On utiliser des attributs spéciaux qui fonctionnent sur toutes les balises : L’attribut <b>class</b>  et  l'attribut <b>id</b>.</p>
<h4>Attribut class</h4>
	<p>Cet attribut peut être mis sur n'importe quelle balise, aussi bien titre que paragraphe, image, etc.</p>
		<h5 class="right">Code : HTML</h5>
			<code>&lth1 class=""&gt &lt/h1&gt <br>
				  &ltp class=""&gt &lt/p&gt <br>
				  &ltimg class="" /&gt
			</code>
				<p>Dans l’attribut class on doit écrire un nom qui sert à identifier la balise. On peut écrire ce qu’on veut à condition le nom commence par une lettre.</p>
<h5 class="right">Exemple : </h5>
	<h5 style="margin-left: 3em">Code HTML :</h5>
		<code>&ltp class="introduction"&gt &lt/p&gt</code>

		<p>Pour utiliser la class "introduction" dans le code CSS, il vous suffit de marquer le nom de la class avec un point devant (.), comme l'exemple ci-dessous.</p>

	<h5 style="margin-left: 3em">Code CSS :</h5>
		<code>.introduction { <br> color: blue; <br> }</code>


<h4>Attribut id</h4>
	<p>Il fonctionne exactement de la même manière que class mais ne peut être utilisé qu'une fois dans le code.</p>
		<p>En pratique, nous ne mettrons des id que sur des éléments qui sont uniques dans la page, comme par exemple le logo :</p>

<h5 class="right">Exemple : </h5>
	<h5 style="margin-left: 3em">Code HTML :</h5>
		<code>&ltimg src="logo.jpg" id="logo"&gt &lt/img&gt</code>

		<p>Pour utiliser l'ID "logo" dans le code CSS, il vous suffit de marquer le nom de l'ID avec un hashtag devant (#), comme l'exemple ci-dessous.</p>

	<h5 style="margin-left: 3em">Code CSS :</h5>
		<code>#logo { <br> width: 250px; <br> }</code>

<h4>Appliquer un style : les balises universelles</h4>
	<p>Il arrivera parfois que vous ayez besoin d'appliquer une class (ou un id) à certains mots qui, à l'origine, ne sont pas entourés par des balises.</p>
	<p>Le problème de class, c'est qu'il s'agit d'un attribut et on peut la mettre que sur une balise</p>
	<p>Il existe deux balises dites <b>universelles</b>, qui n'ont aucune signification particulière :</p>
	<ul>
		<li style="margin-left: 3em;"><b>&ltspan&gt &lt/span&gt:</b> c'est une balise de type <b>inline.</b><br>C’est une balise que l'on place au sein d'un paragraphe de texte, pour sélectionner certains mots uniquement.<br>
		Les balises &ltstrong&gt et &ltem&gt sont de la même famille.</li> <br>

		<li style="margin-left: 3em;"><b>&ltdiv&gt &lt/div&gt:</b> c'est une balise de type <b>block</b>.<br> C'est une balise qui entoure un bloc de texte.<br>
		Les balises &ltp&gt, &lth1&gt, etc sont de la même famille.<br>
		Ces balises ont quelque chose en commun : elles créent un nouveau « bloc » dans la page et provoquent donc obligatoirement un retour à la ligne.
		</li>
	</ul>


</body>
</html>