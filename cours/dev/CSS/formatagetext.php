<!DOCTYPE html>
<html>
<head>
	<?php include "include/design.php"; ?>
</head>
<body>
	<?php include "include/headercss.php"; ?>


<h1 style="text-align: center;">Formatage du texte</h1>
	<p>Le formatage de texte signifie simplement que l'on va modifier l'apparence du texte (on dit qu'on le « met en forme »).</p>
	<p><b>Une police de caractères ne sera pas systématiquement reproduite chez votre visiteur</b> car un navigateur utilise les polices de caractères présentes sur la machine du visiteur, si celles que vous avez programmées n'y sont pas, elles seront remplacée par une police par défaut : Times New Roman sur les PC, Times sur les Mac.</p>
	<p>Ces dernières ne sont pas les plus adaptée (attention aux textes de petite taille par exemple).</p>

<h4>La taille des caractères</h4>
		<p>Pour modifier la taille du texte, on utilise la propriété CSS : <b>font-size</b>.</p>
				<p>Plusieurs techniques vous sont proposées :</p>
					<p>Indiquer une <b>taille absolue</b> : en pixels, en centimètres ou millimètres.</p>
						<p>Cette méthode est très précise mais il est conseillé de ne l'utiliser que si c'est absolument nécessaire, car on risque d'indiquer une taille trop petite pour certains lecteurs.</p>
							<p>Indiquer une <b>taille relative</b> : en pourcentage, « em » ou « ex», cette technique a l'avantage d'être plus souple. Elle s'adapte plus facilement aux préférences de taille des visiteurs.</p>
								<h4>La taille absolue</h4>
									<p>Pour indiquer une taille absolue, on utilise généralement les pixels. Pour avoir un texte de 16 pixels de hauteur, vous devez donc écrire :</p>
										<h5 class="right">Code : CSS</h5>
											<code>font-size: 16px;</code>
												<p>Les lettres auront une taille de 16 pixels, comme le montre la figure suivante.</p>
													<img src="../../../image/css/3_css.jpg" class="archIMG">
<h4>Remarque : Différentes tailles de texte</h4>
	<p>On peut également définir des tailles en centimètres ou millimètres.</p>
		<p>Remplacez « px» par « cm» ou « mm ». Ces unités sont cependant moins bien adaptées aux écrans.</p>
<h4>Une valeur relative</h4>
	<p>C'est la méthode recommandée car le texte s'adapte alors plus facilement aux préférences de tous les visiteurs.</p>
		<p>Il y a plusieurs moyens d'indiquer une valeur relative. Vous pouvez par exemple écrire la taille avec des mots en anglais comme ceux-ci :</p>
<code>xx-small : minuscule ; <br>
x-small : très petit ; <br>
small : petit ;<br>
medium : moyen ;<br>
large : grand ; <br>
x-large : très grand ;<br>
xx-large : gigantesque.<br></code>

<h4>La Police de caractère</h4>
	<p>Pour qu'une police s'affiche correctement, il faut que tous les internautes l'aient. Si un internaute n'a pas la même police que vous, son navigateur prendra une police par défaut (une police standard) qui n'aura peut-être rien à voir avec ce que vous vous attendiez.</p>
		<p>La propriété CSS qui permet d'indiquer la police à utiliser est <b>font-family</b>.</p>
			<p>Vous devez écrire le nom de la police comme ceci :</p>
				<h5 class="right">Code : CSS</h5>
					<code>balise { <br>font-family: police; <br>}</code>
						<p>Seulement, pour éviter les problèmes si l'internaute n'a pas la même police que vous, on précise en général plusieurs noms de police, séparés par des virgules :</p>
							<h5 class="right">Code : CSS</h5>
								<code>balise { <br>font-family: police1, police2, police3, police4; <br>}</code>
									<p>Le navigateur essaiera d'abord d'utiliser la police1. S'il ne l'a pas, il essaiera la police2. S'il ne l'a pas, il passera à la police3, et ainsi de suite.</p>
										<p>Différence entre police sérif et sans sérif</p>
											<img src="../../../image/css/4_css.png" class="archIMG">
												<h4>Exemple :</h4>
													<h5 class="right">Code : CSS</h5>
														<code>p { <br>font-family: Impact, "Arial Black", Arial, Verdana, sans-serif;<br>}</code>
														<P>Cela signifie : « Mets la police Impact ou, si elle n'y est pas, Arial Black, ou sinon Arial, ou sinon Verdana, ou si rien n'a marché, mets une police standard (sans-serif) ».</P>
														<p>Si le nom de la police comporte des espaces, il est conseillé de l'entourer de guillemets,</p>
<h4>Utiliser une police personnalisée avec @font-face</h4>
	<p>Aujourd'hui, avec CSS 3, il existe heureusement un moyen d'utiliser n'importe quelle police sur son site.</p>
	<p>Cela fonctionne bien avec la plupart des navigateurs.</p>
	<p>Mais attention, il y a des défauts :</p>
	<p style="margin-left: 4em;">Il faudra que le navigateur de vos visiteurs télécharge automatiquement le fichier de la police, dont le poids peut atteindre, voire dépasser 1 Mo…</p>
	<p style="margin-left: 4em;">La plupart des polices sont soumises au droit d'auteur, il n'est donc pas légal de les utiliser sur son site.</p>
	<p>Il existe des sites comme <a target="_blank" href="https://www.fontsquirrel.com/">fontsquirrel</a>  et  <a target="_blank" href="http://www.dafont.com/fr/">dafont</a>  qui proposent en téléchargement un certain nombre de polices libres de droits</p>
	<p>Il existe plusieurs formats de fichiers de polices et ceux-ci ne fonctionnent pas sur tous les navigateurs.</p>
	<p>Voici les différents formats de fichiers de polices qui existent et qu'il faut connaître :</p>
	<ul>
		<li style="margin-left: 3.5em;"><b>.ttf :</b> TrueType Font. Fonctionne sur IE9 et tous les autres navigateurs.</li>
		<li style="margin-left: 3.5em;"><b>.eot :</b> Embedded OpenType . Fonctionne sur Internet Explorer uniquement, toutes versions.
Ce format est propriétaire, produit par Microsoft</li>
		<li style="margin-left: 3.5em;"><b>.svg :</b> SVG Font. Le seul format reconnu sur les iPhones et iPads pour le moment.</li>
		<li style="margin-left: 3.5em;"><b>.woff :</b> Web Open Font Format. Nouveau format conçu pour le Web, qui fonctionne sur IE9 et tous les autres navigateurs.</li>
	</ul>	
	<p>En CSS, pour définir une nouvelle police, vous devez la déclarer comme ceci :</p>
	<h4>Exemple :</h4>
	<code>@font-face { <br>
 font-family: "Open Sans"; <br>
 src: url("/fonts/OpenSans-Regular-webfont.woff2") format("woff2"), <br>
        url("/fonts/OpenSans-Regular-webfont.woff") format("woff");<br>}</code>

<p>Le fichier de police doit ici être situé dans le même dossier que le fichier CSS (ou dans un sous-dossier, si vous utilisez un chemin relatif).</p>
<p>La section @font-face permet de définir un nouveau nom de police qui pourra être utilisé dans le fichier CSS. Ensuite, nous utilisons ce nom de police avec la propriété font-family.</p>
<h4>Mise en forme du texte (Italic / oblique)</h4>
	<p><b>font-style</b> qui peut prendre trois valeurs :</p>
	<ul>
		<li style="margin-left: 3.5em;"><b>italic :</b> le texte sera mis en italique</li>
		<li style="margin-left: 3.5em;"><b>oblique :</b> le texte sera passé en oblique (les lettres sont penchées, le résultat est légèrement différent de l'italique proprement dit).</li>
		<li style="margin-left: 3.5em;"><b>normal :</b> le texte sera normal (par défaut). Cela vous permet d'annuler une mise en italique.
Par exemple, si vous voulez que les textes entre &ltem&gt ne soient plus en italique </li>
	</ul>
<h5 class="right">Code : CSS</h5>
<code>h1 { <br> font-style : italic; <br>{</code>
	<h4>Mise en forme du texte (Gras)</h4>
		<p>La mise en gras en CSS peut par exemple s'appliquer aux titres, à certains paragraphes entiers, etc...</p>
			<p>La propriété CSS pour mettre en gras est <b>font-weight</b> et prend les valeurs suivantes :</p>
			<ul>
				<li style="margin-left: 3.5em;"><b>bold :</b> le texte sera en gras;</li>
				<li style="margin-left: 3.5em;"><b>normal :</b> le texte sera écrit normalement (par défaut).</li>
				<li style="margin-left: 3.5em;"><b>valeur :</b> chiffre correspondant à l’épaisseur du caractère.</li>
			</ul>
				<h5 class="right">Code : CSS</h5>
					<code>h1 { <br> font-weight : bold; <br> }</code>
						<br>
							<img src="../../../image/css/5_css.png" class="archIMG">
<h4>Soulignement et autres décorations</h4>
	<p>La propriété CSS associée porte bien son nom : <b>text-decoration.</b></p>
		<ul>
			<li style="margin-left: 3.5em;"><b>underline :</b> le texte est souligné.</li>
			<li style="margin-left: 3.5em;"><b>line-through :</b> le texte est barré en son milieu.</li>
			<li style="margin-left: 3.5em;"><b>overline :</b> texte a un trait au-dessus (ne pas confondre avec surligné).</li>
			<li style="margin-left: 3.5em;"><b>blink :</b> le texte clignote (sauf ie, Chrome et safari). Valeur désactivée par navigateur pour des raisons d'accessibilité.</li>
			<li style="margin-left: 3.5em;"><b>none :</b> normal (par défaut).</li>
		</ul>
<h4>Alignement</h4>
	<p>Le langage CSS nous permet de faire tous les alignements connus : à gauche, centré, à droite et justifié.</p>
		<p>On utilise la propriété <b>text-align</b> et on indique l'alignement désiré :</p>
			<ul>
				<li style="margin-left: 3.5em;"><b>left :</b> le texte sera aligné à gauche (c'est le réglage par défaut).</li>
				<li style="margin-left: 3.5em;"><b>center :</b> le texte sera centré</li>
				<li style="margin-left: 3.5em;"><b>right :</b> le texte sera alligné a droite.</li>
				<li style="margin-left: 3.5em;"><b>justify :</b> le texte sera « justifié ». Justifier le texte permet de faire en sorte qu'il prenne toute la largeur possible sans laisser d'espace blanc à la fin des lignes.</li>
			</ul>		
				<p>L'alignement ne fonctionne que sur des balises de type block (&ltp&gt, &ltdiv&gt, &lth1&gt, &lth2&gt, …) et c'est un peu logique, quand on y pense : on ne peut pas modifier l'alignement de quelques mots au milieu d'un paragraphe!</p>
				<p>C'est donc en général le paragraphe entier qu'il vous faudra aligner.</p>
<h4>Modification de casse</h4>
	<p>La propriété CSS <b>text-transform</b> permet de modifier la casse des caractères.</p>
		<ul>
			<li style="margin-left: 3.5em;"><b>uppercase :</b> le texte est forcé en majuscule.</li>
			<li style="margin-left: 3.5em;"><b>lowercase :</b> le texte est forcé en minuscule.</li>
			<li style="margin-left: 3.5em;"><b>capitalize :</b> première lettre de chaque mot est forcée en majuscule.</li>
			<li style="margin-left: 3.5em;"><b>none :</b> normal (par défaut).</li>
		</ul>
<h4>Habillage des listes</h4>
	<p>Style de la puce ou le type de numérotation : <b>list-style-type.</b></p>
		<p>Pour une liste non ordonnée, les valeurs possibles sont :</p>
			<ul>
				<li style="margin-left: 3.5em;"><b>disc</b></li>
				<li style="margin-left: 3.5em;"><b>circle</b></li>
				<li style="margin-left: 3.5em;"><b>square</b></li>
				<li style="margin-left: 3.5em;"><b>none</b></li>
			</ul>
				<p>Pour une liste ordonnée, les valeurs possibles sont :</p>
					<ul>
						<li style="margin-left: 3.5em;"><b>decimal </b> (1, 2...)</li> 
						<li style="margin-left: 3.5em;"><b>lower-roman</b> (i,ii...)</li>
						<li style="margin-left: 3.5em;"><b>upper-roman </b> (I, II...)</li>
						<li style="margin-left: 3.5em;"><b>lower-alpha</b> (a, b...), </li>
						<li style="margin-left: 3.5em;"><b>upper-alpha</b> (A, B...)</li>
						<li style="margin-left: 3.5em;"><b>none</b></li>
					</ul>
						<p>Il est possible de spécifier une image comme puce dans une liste : list-style-image Il faut indiquer l'URL de l'image à utiliser.</p>
							<h5>Code : CSS</h5>
								<code>ul { <br> list-style-image : url(formatage/fleche.jpg); <br>}</code>
<h4>Habillage d’un texte autour d’un élément</h4>
	<p>Le CSS nous permet de faire flotter un élément autour du texte. On dit aussi qu'on fait un « habillage ».</p>
		<p style="float: right;"> <br> <br> Une image flottante entourée par du texte <br>Cette propriété float peut prendre deux valeurs très simples :
		<br> <b>left</b> : l'élément flottera à gauche. <br>
		<b>right</b> : l'élément flottera à droite ! L'utilisation des flottants est très simple :</p>
			<img src="../../../image/css/6_css.png" class="archIMG">
				<p>Vous appliquez un float à une balise, puis vous continuez à écrire du texte à la suite normalement.</p>
						<b style="margin-left: 3em;">On peut aussi bien utiliser la propriété float sur des balises block que sur des balises inline.</b> <br>
<h4>Stopper un flottant</h4>
	<p>Quand vous mettez en place un flottant, le texte autour l'habille. On aimerait pouvoir obtenir le même résultat qu'à la figure suivante. Le texte sous l'image ignore la propriété float</p>
		<img src="../../../image/CSS/7_css.jpg" class="archIMG">
			<p>Il existe en fait une propriété CSS qui permet de dire : « Stop, ce texte doit être en-dessous du flottant et non plus à côté ».<br> C'est la propriété <b>clear: both</b>.</p>
</body>
</html>