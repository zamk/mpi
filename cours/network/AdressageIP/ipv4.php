<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <title>MPI | Réseau</title>
    <link rel="stylesheet" type="text/css" href="../../../CSS/network.css">
    <link rel="stylesheet" type="text/css" href="../../../fonts/icomoon/style.css">

</head>
<body>
    <nav class="bgc1 clearfix">
        <h1 class="mhmm mvm">
            <span>
                <a style="color:black;" href="../../../index.php">MPI</a> > <a style="color:black;" href="../../network.php">Réseau</a> > Adressage IP > Les adresses IPv4
            </span>
            <span>
				<a style="float: right; margin-right: 0.5em;" href="../../linux.php">Linux</a>
			</span>
			<span>	
				<a style="float: right; margin-right: 1em;" href="../../network.php">Réseau</a>
			</span>
			<span>	
				<a style="float: right; margin-right: 1em;" href="../../dev.php">Développement</a>
            </span>        
        </h1>
    </nav>

    <h1 style="text-align: center;">Les adresses IP v4</h1>
    <p>Dans la version 4 du protocole IP, les adresses sont codées sur 32 bits. Elles sont représentées sous forme de 4 nombres décimaux allant de 0 à 255 séparés par des points.</p>
    <b class="Margin">Exemple</b>&nbsp;: <code>176.26.142.26</code>
    <h1>Les classes d'adresses IP</h1>
    <p>Toutes les adresses IP sont réparties sous différentes classes. À chaque classe correspond un nombre déterminé de bits pour le réseau et pour la machine. Ce sont principalement les classe A à C qui sont couramment utilisées. Les classes D et E sont réservées à des usages particuliers.</p>

    <h1>Classe A</h1>
    <p>Les adresses dont le premier bit est <code>0</code> sont de la classe A. En binaire, nous aurons les adresses du type suivant :</p>
    <code class="Margin">0NNNNNNN.HHHHHHHH.HHHHHHHH.HHHHHHHH</code>
    <p>Les 8 premiers bits correspondent à la partie réseau et les autres à la partie machine. Les valeurs du premier octet de la classe A iront donc de 0 à 127. Avec des adresses de classe A, nous aurons ainsi peu de réseaux mais de très grande taille. Nous retrouverons ces adresses principalement sur des backbone.</p>
    <b class="Margin">Exemple</b>&nbsp;: <code>114.50.49.13</code>

    <h1>Classe B</h1>
    <p>Les adresses dont les deux premiers bits sont <code>10</code> sont de la classe B. En binaire, nous aurons les adresses du type suivant :</p>
    <code class="Margin">10NNNNNN.NNNNNNNN.HHHHHHHH.HHHHHHHH</code>
    <p>Les 16 premiers bits correspondent à la partie réseau et les autres à la partie machine. Les valeurs du premier octet de la classe B iront donc de 128 à 191.</p>
    <b class="Margin">Exemple</b>&nbsp;: <code>176.26.142.26</code>

    <h1>Classe C</h1>
    <p>Les adresses dont les trois premiers bits sont <code>110</code> sont de la classe C. En binaire, nous aurons les adresses du type suivant :</p>
    <code class="Margin">110NNNNN.NNNNNNNN.NNNNNNNN.HHHHHHHH</code>
    <p>Les 24 premiers bits correspondent à la partie réseau et les autres à la partie machine. Les valeurs du premier octet de la classe C iront donc de 192 à 223. Avec des adresses de classe C, nous aurons ainsi beaucoup de réseaux de petite taille. Nous retrouverons ces adresses chez les particuliers ou sur les LAN.</p>
    <b class="Margin">Exemple</b>&nbsp;: <code>192.168.1.34</code>

    <h1>Classe D</h1>
    <p>Les adresses dont les quatre premiers bits sont <code>1110</code> sont de la classe D. En binaire, nous aurons les adresses du type suivant :</p>
    <code class="Margin">1110XXXX.XXXXXXXX.XXXXXXXX.XXXXXXXX</code>
    <p>Les valeurs du premier octet de la classe D iront donc de 224 à 239. Ces adresses sont réservées pour les communications multicast.</p>
    <b class="Margin">Exemple</b>&nbsp;: <code>226.26.12.126</code>

    <h1>Classe E</h1>
    <p>Les adresses dont les quatre premiers bits sont <code>1111</code> sont de la classe E. En binaire, nous aurons les adresses du type suivant :</p>
    <code class="Margin">1111XXXX.XXXXXXXX.XXXXXXXX.XXXXXXXX</code>
    <p>Les valeurs du premier octet de la classe D iront donc de 240 à 255. Ces adresses sont réservées à des usages particuliers (indéterminé).</p>
    <b class="Margin">Exemple</b>&nbsp;: <code>246.168.1.34</code>
    <br>
    <br>







</body>
</html>