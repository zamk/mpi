<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <title>MPI | Réseau</title>
    <link rel="stylesheet" type="text/css" href="../../../CSS/network.css">
    <link rel="stylesheet" type="text/css" href="../../../fonts/icomoon/style.css">

</head>
<body>
    <nav class="bgc1 clearfix">
        <h1 class="mhmm mvm">
            <span>
                <a style="color:black;" href="../../../index.php">MPI</a> > <a style="color:black;" href="../../network.php">Réseau</a> > Adressage IP > Les adresses particulières
            </span>
            <span>
				<a style="float: right; margin-right: 0.5em;" href="../../linux.php">Linux</a>
			</span>
			<span>	
				<a style="float: right; margin-right: 1em;" href="../../network.php">Réseau</a>
			</span>
			<span>	
				<a style="float: right; margin-right: 1em;" href="../../dev.php">Développement</a>
            </span>        
        </h1>
    </nav>

<h1 style="text-align: center;">Les adresses particulières</h1>
    <p>Le protocole IP définit un certain nombre d'adresses particulières. En voici un aperçu.</p>
<h1>L'adresse zéro</h1>
    <p>En <b>IP v4</b>, l'adresse zéro (<code>0.0.0.0</code>) signifie "<b>tout le réseau</b>". Il s'agit en fait d'une adresse réseau. En <b>IP v6</b>, l'adresse zéro (<code>::</code>) indique une <b>adresse indéfinie</b>, c'est à dire une absence d'adresse.</p>
<h1>L'adresse de bouclage (loopback)</h1>
    <p>Une adresse de bouclage (loopback en anglais) est une adresse utilisée par une interface pour s'envoyer un message à elle-même. Elle peut, par exemple, être utilisée lors de tests.</p>
    <p>En IP v4, il s'agit de l'adresse <code>127.0.0.1</code>. En IP v6, il s'agit de l'adresse <code>::1</code>.</p>
<h1>Les adresses locales</h1>
    <p>Les adresses locales sont des adresses qui, normalement, ne sont pas retransmises par les routeurs. Elles ne devraient donc jamais se retrouver sur un réseau global comme internet.</p>
<h1>IP v4</h1>
    <p>En IP v4, les adresses locales sont surtout utilisées derrière un routeur NAT. C'est en général ce genre d'adresse que vous avez chez vous à la maison. Il existe des adresses locales pour les classe A, B et C. Les voici.</p>

<table style="margin-left: 2.9em;
  border-collapse : separate;
  border-spacing : 10px;">
  <thead>
    <tr>
      <th>Classe</th>
      <th>Adresse réseau</th>
      <th>Première adresse</th>
      <th>Dernière adresse</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>A</td>
      <td><code>10.0.0.0/8</code></td>
      <td><code>10.0.0.0</code></td>
      <td><code>10.255.255.255</code></td>
    </tr>
    <tr>
      <td>B</td>
      <td><code>172.16.0.0/12</code></td>
      <td><code>172.16.0.0</code></td>
      <td><code>172.31.255.255</code></td>
    </tr>
    <tr>
      <td>C</td>
      <td><code>192.168.0.0/16</code></td>
      <td><code>192.168.0.0</code></td>
      <td><code>192.168.255.255</code></td>
    </tr>
  </tbody>
</table>

<h1>IP v6</h1>
  <p>En IP v6, il est défini deux type d'adresses locales : les adresses locales de lien (link-local) et les adresses locales globalement unique.</p>
    <h1>Les adresses locales de lien (link-local)</h1>
      <p>Ces adresses sont définies pour être utilisées sur des lien n'ayant pas de routeurs, pour des configurations automatiques d'adresses ou la recherche de voisins. Ces adresses commencent par les bits <code>1111111010</code> suivis de 54 zéros et de l'adresse hôte. Il s'agit donc des adressses <code>fe80::/64</code>.
</p>
<table style="margin-left: 2.9em;
  border-collapse : separate;
  border-spacing : 10px;">
  <thead>
    <tr>
      <th>10 bits</th>
      <th>54 bits</th>
      <th>64 bits</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><code>1111111010</code></td>
      <td><code>00000000...00000000</code></td>
      <td><code>adresse hôte</code></td>
    </tr>
  </tbody>
</table>

<h1>Les adresses locales globalement unique</h1>
  <p>Ces adresses ont une portée plus grande que les adresses locales de lien. Elles peuvent par exemple être utilisées au sein d'une entreprise. Ces adresses commencent par les bits <code>11111101</code> suivis d'un identifiant global de 40 bits généré aléatoirement, de 16 bits d'adresse réseau et de 64 bits d'adresse hôte. Il s'agit donc des adressses <code>fd00::/8</code>.</p>

<table style="margin-left: 2.9em;
  border-collapse : separate;
  border-spacing : 10px;">
  <thead>
    <tr>
      <th>8 bits</th>
      <th>40 bits</th>
      <th>16 bits</th>
      <th>64 bits</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><code>11111101</code></td>
      <td><code>identifiant global</code></td>
      <td><code>adresse réseau</code></td>
      <td><code>adresse hôte</code></td>
    </tr>
  </tbody>
</table>  
<br>

</body>
</html>