<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <title>MPI | Réseau</title>
    <link rel="stylesheet" type="text/css" href="../../../CSS/network.css">
    <link rel="stylesheet" type="text/css" href="../../../fonts/icomoon/style.css">

</head>
<body>
    <nav class="bgc1 clearfix">
        <h1 class="mhmm mvm">
            <span>
                <a style="color:black;" href="../../../index.php">MPI</a> > <a style="color:black;" href="../../network.php">Réseau</a> > Adressage IP >  Sous-réseau
            </span>
            <span>
				<a style="float: right; margin-right: 0.5em;" href="../linux.php">Linux</a>
			</span>
			<span>	
				<a style="float: right; margin-right: 1em;" href="../network.php">Réseau</a>
			</span>
			<span>	
				<a style="float: right; margin-right: 1em;" href="../dev.php">Développement</a>
            </span>        
        </h1>
    </nav>

<h1 style="text-align: center;">Les sous-réseaux</h1>
    <p>Comme nous l'avons vu, l'adresse IP possède une partie réseau et une partie hôte. Pour ce qui est de l'IP v4, les adresses ont une partie réseau de 8, 16 ou 24 bits suivant la classe d'adresse. Pour ce qui est de l'IP v6, les adresses ont une partie réseau de 64 bits.</p>
    <p>Ces masques par défaut ne permettent pas toujours de coller aux besoins particuliers de tout le monde. C'est pourquoi, il est possible de subdiviser les réseaux par défaut au moyen d'un masque de sous-réseau.</p>
    <p>Un masque de sous-réseau est un nombre de la même taille que l'adresse. Les premiers bits sont à <code>1</code> et désignent la partie réseau. Les derniers bits sont à <code>0</code> et désignent la partie hôte.</p>
        <b class="Margin">Exemple</b>&nbsp;: <code>111111...111111000000...000000</code>
<h1>Notation</h1>
    <p>En IP v4, les masques de réseau peuvent se représenté comme une adresse réseau.</p>
        <b class="Margin">Exemple</b>&nbsp;: <code>11111111111111111111111111100000</code> peut s'écrire <code>255.255.255.224</code> en IP v4.
    <p>Une autre notation utilisée en IP v4 et seule valable en IP v6 est de faire suivre l'adresse IP d'une barre oblique / et ensuite du nombre de bit du masque de réseau.</p>
        <b class="Margin">Exemple</b>&nbsp;: <code>11111111111111111111111111100000...</code> peut s'écrire <code>/27</code>
<h1>Subdivision de réseaux</h1>
    <p>Lorsque nous avons à subdiviser des réseaux, il y a deux approches possibles. Soit nous essayons de déterminer le nombre de sous-réseaux que nous voulons obtenir, soit nous essayons de déterminer le nombre de machines par sous-réseaux.</p>
<h1>Subdivision sur base du nombre de sous-réseaux</h1>
    <p>Dans ce cas-ci, nous allons agrandir le masque réseau (bits à 1) d'autant de bit qu'il est nécessaire pour obtenir le nombre de subdivisions voulu. Voici un petit tableau explicatif.</p>

<table style="margin-left: 2.9em;
  border-collapse : separate;
  border-spacing : 10px;
  width: 60%;">
  <thead>
    <tr>
      <th>Nombre de subdivisions</th>
      <th>Nombre de bits</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>2</td>
      <td>1</td>
    </tr>
    <tr>
      <td>3 à 4</td>
      <td>2</td>
    </tr>
    <tr>
      <td>5 à 8</td>
      <td>3</td>
    </tr>
    <tr>
      <td>9 à 16</td>
      <td>4</td>
    </tr>
    <tr>
      <td>17 à 32</td>
      <td>5</td>
    </tr>
    <tr>
      <td colspan="2">etc.</td>
    </tr>
  </tbody>
</table>

<p>Par exemple, si nous voulons subdiviser un réseau en 350, nous devrons agrandir le masque de 9 bits (8 bits nous donnent 256 subdivisions et 9 bits 512).</p>
<p>Ainsi, en IP v4, si nous avons une adresse de classe B qui a un masque par défaut de 16 bits, nous aurons un masque de sous-réseau de 16+9 bits, soit 25 bits.</p>
<p>En IP v6, si nous avons un masque standard de 64 bits, nous aurons un masque de sous-réseau de 64+9 bits, soit 73 bits.</p>
    <h1>Subdivision sur base du nombre d'hôtes</h1>
<p>Dans ce cas-ci, nous allons garder pour la partie machine (bits à 0) autant de bit qu'il est nécessaire pour obtenir le nombre de machine moins deux (l'adresse réseau et l'adresse de diffusion). Voici un petit tableau explicatif.</p>







</body>
</html>