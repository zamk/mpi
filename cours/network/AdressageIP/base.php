<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <title>MPI | Réseau</title>
    <link rel="stylesheet" type="text/css" href="../../../CSS/network.css">
    <link rel="stylesheet" type="text/css" href="../../../fonts/icomoon/style.css">

</head>
<body>
    <nav class="bgc1 clearfix">
        <h1 class="mhmm mvm">
            <span>
                <a style="color:black;" href="../../../index.php">MPI</a> > <a style="color:black;" href="../../network.php">Réseau</a> > Adressage IP > Concepts de base
            </span>
            <span>
				<a style="float: right; margin-right: 0.5em;" href="../../linux.php">Linux</a>
			</span>
			<span>	
				<a style="float: right; margin-right: 1em;" href="../../network.php">Réseau</a>
			</span>
			<span>	
				<a style="float: right; margin-right: 1em;" href="../../dev.php">Développement</a>
            </span>        
        </h1>
    </nav>

    <h1 style="text-align: center;">Concepts de base</h1>
    <p>Pour pouvoir recevoir des informations, chaque machine connecté sur un réseau doit pouvoir être identifié de manière unique. On utilisera à cet effet un identifiant. Dans le cadre des réseaux TCP/IP, ces derniers porteront le nom d'adresses IP. Il s'agit en fait d'un simple numéro qui doit être unique sur l'entièreté du réseau.</p>
    <p>Dans un souci de performance, un réseau TCP/IP sera subdivisé en sous-réseaux. Ainsi, une adresse IP possédera deux parties : une partie réseau situé au début de l'adresse et une partie hôte située à la fin de l'adresse.</p>

        <b class="Margin">Exemple</b>&nbsp;: <code>NN...NNHH...HH</code>
    <ul>
      <li class="Margin"><code>N</code> représente un bit d'adresse réseau</li>
      <li class="Margin"><code>H</code> représente un bit d'adresse hôte</li>
    </ul>
<p>Les machines se trouvant sur un même sous-réseau communiqueront entre elle de manière directe. Les machines se trouvant sur des sous-réseaux différents devront passer par des routeurs. Si nous comparons cette situation à la poste classique, les sous-réseaux sont des rues. Si vous voulez envoyer une lettre à une personne habitant dans votre rue, vous la déposez directement dans sa boîte aux lettres. Sinon, vous la confiez à un facteur (le routeur) qui se chargera de la parvenir au destinataire.</p>







</body>
</html>