<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <title>MPI | Réseau</title>
    <link rel="stylesheet" type="text/css" href="../../../CSS/network.css">
    <link rel="stylesheet" type="text/css" href="../../../fonts/icomoon/style.css">

</head>
<body>
    <nav class="bgc1 clearfix">
        <h1 class="mhmm mvm">
            <span>
                <a style="color:black;" href="../../../index.php">MPI</a> > <a style="color:black;" href="../../network.php">Réseau</a> > Adressage IP > Introduction
            </span>
            <span>
				<a style="float: right; margin-right: 0.5em;" href="../../linux.php">Linux</a>
			</span>
			<span>	
				<a style="float: right; margin-right: 1em;" href="../../network.php">Réseau</a>
			</span>
			<span>	
				<a style="float: right; margin-right: 1em;" href="../../dev.php">Développement</a>
            </span>        
        </h1>
    </nav>


    <h1 style="text-align: center;">Introduction</h1>
        <p>Au début de l'informatique, chaque constructeur d'ordinateurs utilisait ses propres standards. Il était virtuellement impossible, ou tout du moins extrêmement difficile, d'interconnecter des machines de marques différentes.</p>

        <p>Pour remédier à ce problème le gouvernement américain décide de créer un protocole permettant l'interconnexion de toutes ces machines. De cette décision allait naître TCP/IP et internet.</p>

        <p>Une simple requête sur un moteur de recherche vous permettra de trouver une pléthore de sites vous expliquant les tenants et aboutissant du TCP/IP et de l'internet. Je voudrais ici me concentrer sur l'adressage IP.</p>





</body>
</html>