<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <title>MPI | Réseau</title>
    <link rel="stylesheet" type="text/css" href="../../../CSS/network.css">
    <link rel="stylesheet" type="text/css" href="../../../fonts/icomoon/style.css">

</head>
<body>
    <nav class="bgc1 clearfix">
        <h1 class="mhmm mvm">
            <span>
                <a style="color:black;" href="../../../index.php">MPI</a> > <a style="color:black;" href="../../network.php">Réseau</a> > Adressage IP > Les adresses IPv6
            </span>
            <span>
				<a style="float: right; margin-right: 0.5em;" href="../../linux.php">Linux</a>
			</span>
			<span>	
				<a style="float: right; margin-right: 1em;" href="../../network.php">Réseau</a>
			</span>
			<span>	
				<a style="float: right; margin-right: 1em;" href="../../dev.php">Développement</a>
            </span>        
        </h1>
    </nav>

    <h1 style="text-align: center;">Les adresses IP v6</h1>
    <p>Avec l'essor d'internet, nous nous sommes vite retrouvé à cours d'adresses IP v4. Ainsi, il a fallu trouver des solutions. Dans la version 6 du protocole IP, les adresses sont maintenant codées sur 128 bits au lieu de 32. Nous avons considérablement augmenté le nombre d'adresses et chaque appareil peut maintenant recevoir la sienne.</p>
    <h1>Représentation des adresses</h1>
    <p>Le format des adresses est un peu différente dans la version 6 que dans la version 4. Ici, elles sont formées de 8 nombres hexadécimaux de 4 chiffres séparés par des deux-points.</p>
    <b class="Margin">Exemple</b>&nbsp;: <code>abcd:ef01:2345:6789:abcd:ef01:2345:6789</code>
    <p>Il existe un certain nombre de règles pour la représentation des adresses IP v6.</p>
    <p>Les symboles hexadécimaux <code>a</code> à <code>f</code> doivent être représentés par des minuscules.</p>
    <p>Les premiers zéros de chaque nombre doivent être omis (mais pas les derniers).</p>
    <p>
    <b>Exemple</b>&nbsp;: <code>a123:<b>0:0</b>:def0:1234:<b>0:0</b>:def0</code>
    doit s'écrire <code>a123<b>::</b>def0:1234:0:0:def0</code> ou <code>a123:0:0:def0:1234<b>::</b>def0</code>
    </p>
    <p>Une suite de plusieurs nombres égales à zéros (et une seule) doit être omise. S'il est possible se supprimer plusieurs suites de zéros, la suite la plus longue sera supprimée. S'il n'y a qu'un seul nombre égale à zéro, il sera représenté par un seul zéro.</p>

    <p>
    <b>Exemple</b>&nbsp;: <code>a123:<b>0:0</b>:def0:1234:<b>0:0</b>:def0</code>
    doit s'écrire <code>a123<b>::</b>def0:1234:0:0:def0</code> ou <code>a123:0:0:def0:1234<b>::</b>def0</code>
    </p>
    <p>
    <b>Exemple</b>&nbsp;: <code>a123:<b>0:0:0</b>:def0:1234:0:def0</code>
    doit s'écrire <code>a123<b>::</b>def0:1234:0:def0</code>
    </p>
    <p>
    <b>Exemple</b>&nbsp;: <code>abcd:ef01:<b>0</b>:6789:abcd:ef01:2345:6789</code>
    </p>
    <h1>Les types d'adresse</h1>
    <p>Dans l'IP v6, la notion de classe de l'IP v4 est abandonnées. Les adresses sont réparties suivant leur usage. La partie réseau de l'adresse est constituée des 64 premiers bits et la partie hôte des 64 derniers.</p>
    
    <h4>Adresse Unicast</h4>
        <p>Il s'agit d'une adresse "normale". Elle désigne une seule machine.</p>
    <h4>Adresses anycast</h4>
        <p>Une adresse anycast représente un ensemble de machines. Lorsqu'un message sera envoyé sur cette adresse, la machine la plus proche le recevra.</p>
    <h4>Adresses multicast</h4>
    <p>
        Comme pour les adresses anycast, une adresse multicast représente un ensemble de machines. Toutefois, lorsqu'un message sera envoyé sur cette adresse, toutes les machines du groupe le recevront. Les huits premiers bits de ces adresses sont à <code>1</code>. Ce sont donc le adresses qui vont de <code>ff00::</code> à <code>ffff:ffff:ffff:ffff:ffff:ffff:ffff:ffff</code>
    </p>








</body>
</html>