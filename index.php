<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>MPI | Accueil</title>
	<link rel="stylesheet" type="text/css" href="CSS/index.css">
	<link rel="stylesheet" type="text/css" href="CSS/footer.css">
</head>
<body>

<nav class="bgc1 clearfix">
    <h1 class="mhmm mvm">
		<span style="color: black">MPI</span>
			<span>
				<a style="float: right; margin-right: 0.5em;" href="cours/linux.php">Linux</a>
			</span>
			<span>
				<a style="float: right; margin-right: 1em;" href="cours/network.php">Réseau</a>
			</span>
			<span>
				<a style="float: right; margin-right: 1em;" href="cours/dev.php">Développement</a>
			</span>
	</h1>
</nav>

<h2 style="text-align: center;">Bienvenue sur MPI !</h2>
	<p style="text-align: center;">Sur ce site internet vous trouverez des cours d'informatique détaillés pour un apprentissage rapide.<br> 
	Plusieurs catégories s'ouvrent à vos envies, vous pouvez travailler du réseau, du développement ou plonger dans le monde de Linux.</p>
		<p style="text-align: center;">Le code du site sera totalement libre d'accès, <a target="_blank" href="https://gitlab.com/zamk/mpi">cliquer ici</a> pour voir la page du projet.</p>
			


		<h3 style="text-align: center;
				   margin-top: 29em;"><a href="contact.php">Cliquer ici pour me contactez !</a></h3>


				
</body>
</html>
